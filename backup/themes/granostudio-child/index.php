<?php get_header(); ?>

<!-- Div banner -->
<div class="div1" style="margin-top: 21px;">
  <?php module_bannerconteudo(23, 1) ?>
</div>
<!-- fim div1 -->

<!-- Div soluções -->
<div class="div2">

  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Soluções para o seu negócio</h1>
        <hr class="titulo">
      </div>
    </div>

    <div class="row" style="margin-top: 50px;margin-bottom: 60px;">
      <div class="col-sm-2 col-sm-offset-2">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice6@2x.png">
        <p class="paragrafo-div">Soluções de negócio fim a fim para Seguradoras</p>
      </div>
      <div class="col-sm-2">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice7@2x.png">
        <p class="paragrafo-div">Integração com grande marcas globais</p>
      </div>
      <div class="col-sm-2">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice8@2x.png">
        <p class="paragrafo-div">Consultoria</p>
      </div>
      <div class="col-sm-2">
        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice9@2x.png">
        <p class="paragrafo-div">Serviços</p>
      </div>

    </div>

    <button type="button" class="botao" name="button">Nossas soluções</button>
  <!-- fim div container -->
  </div>
<!-- fim div2 -->
</div>

<!-- Div destaque -->
<div class="div3">

          <?php
          // start by setting up the query
          $query = new WP_Query( array(
              'post_type' => 'post',
              'posts_per_page' => 1,
          ));

          // now check if the query has posts and if so, output their content in a banner-box div
          if ( $query->have_posts() ) { ?>

                  <?php while ( $query->have_posts() ) : $query->the_post();
                  $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                    <div class="col-sm-3 col-sm-offset-1 col-md-offset-2 col-noticias-left">

                      <ul>
                        <?php
                        foreach((get_the_category()) as $category) {
                          echo '<li class="categoria-post">' . $category->cat_name . '</li>';
                        }
                        ?>
                      </ul>
                      <a href="<?php echo esc_url( $url ); ?>"><p class="titulo-post"><?php the_title() ?></p></a>
                      <p class="excerpt-post"><?php echo the_excerpt_max_charlength(55); ?></p>
                      <a href="<?php echo get_the_permalink(); ?>"><button type="button" class="botao" name="button">Entenda como</button></a>

                    </div>
                  <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>


          <?php
            // start by setting up the query
            $query = new WP_Query( array(
                'post_type' => 'post',
                'offset' => 1,
                'posts_per_page' => 1,
            ));

            // now check if the query has posts and if so, output their content in a banner-box div
            if ( $query->have_posts() ) { ?>

                  <?php while ( $query->have_posts() ) : $query->the_post();
                  $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                    <div class="col-sm-5 col-md-4 col-lg-3 col-sm-offset-2 col-noticias-right">

                      <a href="<?php echo esc_url( $url ); ?>"><p><?php the_title() ?></p></a>
                      <p><?php echo the_excerpt_max_charlength(130); ?></p>
                      <a href="<?php echo get_the_permalink(); ?>"><button type="button" class="botao" name="button">Entenda como</button></a>

                    </div>
                  <?php endwhile; ?>

            <?php }
            wp_reset_postdata();
            ?>

<!--   <div class="col-sm-3 col-sm-offset-1 col-md-offset-2 col-noticias-left">
    <p>Tendências</p>
    <p>A transformação digital impacta</p>
    <p>diretamente o mercado de Seguros, no Brasil e no mundo</p>
    <button type="button" class="botao" name="button">Entenda como</button>
  </div>  -->

<!--   <div class="col-sm-5 col-md-4 col-lg-3 col-sm-offset-2 col-noticias-right">
    <p>67% dos CEOs de Seguradoras</p>
    <p>veem criatividade e inovação como algo muito importante para suas organização - mais do que a média dos demais setores</p>
    <button type="button" class="botao" name="button">Entenda como</button>
  </div> -->

</div>
<!-- fim div3 -->

<!-- Div parceiros -->
<div class="div4">
  <div class="container">

    <div class="row" style="margin-bottom: 50px;">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Parceiros de primeira classe</h1>
        <hr class="titulo">
      </div>
    </div>

    <div id="main_area">
        <!-- Slider -->
        <div class="row">
          <div class="col-sm-12" id="slider">
            <!-- Top part of the slider --> 
              <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-5" id="carousel-bounding-box">
                  <div class="carousel slide" id="myCarousel">
                      <!-- Carousel items -->
                      <div class="carousel-inner">
                        <div class="active item" data-slide-number="0">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice1@2x.png"></div>

                        <div class="item" data-slide-number="1">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice2@2x.png"></div>

                        <div class="item" data-slide-number="2">
                        <img src="<?php echo get_stylesheet_directory_uri();?>/img/slice3@2x.png"></div>

                      </div><!-- Carousel nav -->
                      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                      </a>
                  </div>
                </div>

            <div class="col-sm-6" id="carousel-text"></div>

              <div id="slide-content" class="slide-content" style="display: none;">
                <div id="slide-content-0">
                  <h2>Plataforma tecnológica de transformação digital presente nas maiores Seguradoras do mundo.</h2>
                  <p>Abordagem completa para desenhar e ativar aplicações ominichannel em tempo recorde, com foco em Customer Services, Marketing, Sales dentre outras.</p>
                </div>

                <div id="slide-content-1">
                  <h2>Soluções de inteligência analítica para identificar fatores críticos, acompanhamento da execução de metas,</h2>
                  <p>simulações, executive mining, dashboard mining e cockpit mining Itegrável a midias sociais (Facebook, Instagram, Twitter, YouTube, LinkedIn, Reclame Aqui).</p>
                </div>

                <div id="slide-content-2">
                  <h2>A Sistran Brasil é Microsoft Gold Certified Partner e trabalha</h2>
                  <p>com a fabricante para prover soluções utilizando softwares de última geração.</p>
                </div>

              </div>
            </div>
          </div>
        </div><!--/Slider-->

        <div class="row" id="slider-thumbs">
          <!-- Bottom switcher of slider -->
            <ul class="hide-bullets col-sm-6 col-md-4">
              <li class="col-sm-4">
                <a class="thumbnail" id="carousel-selector-0"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice1@2x.png"></a>
              </li>

              <li class="col-sm-4">
                <a class="thumbnail" id="carousel-selector-1"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice2@2x.png"></a>
              </li>

              <li class="col-sm-4">
                <a class="thumbnail" id="carousel-selector-2"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice3@2x.png"></a>
              </li>

            </ul>
            <ul class="col-sm-6">
              <a href="http://sistran.tempsite.ws/index.php/parcerias/"><button type="button" class="botao" name="button">Conheça todos nossos parceiros</button></a>
            </ul>
        </div>
    </div>


  </div>
</div>
<!-- fim div4 -->

<!-- Div blog -->
<div class="div5">
    <div class="container">

      <div class="row" style="margin-top: 100px;margin-bottom: 80px;">
        <div class="col-sm-6 col-sm-offset-3">
          <h1>Blog e Imprensa</h1>
          <hr class="titulo">
        </div>
      </div>

      <div class="banner">

      <div class="row categorias-blog">
        <button type="button" class="botao" name="button">Todos</button>
        <button type="button" class="botao" name="button">Posts</button>
        <button type="button" class="botao" name="button">Imprensa</button>
      </div>

      <div class="col-sm-8">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <!-- <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol> -->

        <!-- Wrapper for slides -->
        <div class="carousel-inner">

          <?php
          // start by setting up the query
          $query = new WP_Query( array(
              'post_type' => 'banner-blog',
              'posts_per_page' => 1,
          ));

          // now check if the query has posts and if so, output their content in a banner-box div
          if ( $query->have_posts() ) { ?>

                  <?php while ( $query->have_posts() ) : $query->the_post();
                  $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                    <div class="item active">

                      <div class="descricao-banner">
                        <ul class="lista-categoria">
                          <?php
                          foreach((get_the_category()) as $category) {
                            echo '<li class="hashtags">' . $category->cat_name . '</li>';
                          }
                          ?>
                        </ul>
                        <a href="<?php echo get_the_permalink(); ?>"><h2><?php the_title() ?></h2></a>
                        <p style="font-size:15px;"><?php echo the_excerpt_max_charlength(150); ?></p>
                        <!-- <a href="<?php echo esc_url( $url ); ?>"><div class="botao"><p>Leia Mais</p></div></a> -->
                      </div>
                      <div class="img-banner">
                        <?php the_post_thumbnail( ); ?>
                      </div>

                    <div class="slide-progress"></div>
                    </div>
                  <?php endwhile; ?>

          <?php }
          wp_reset_postdata();
          ?>

            <!--  Segundo Loop -->

            <?php
            // start by setting up the query
            $query = new WP_Query( array(
                'post_type' => 'banner-blog',
                'offset' => 1,
            ));

            // now check if the query has posts and if so, output their content in a banner-box div
            if ( $query->have_posts() ) { ?>

                    <?php while ( $query->have_posts() ) : $query->the_post();
                        $url = get_post_meta( get_the_ID(), '_url', 1 ); ?>

                      <div class="item">

                        <div class="descricao-banner">
                          <a href="<?php echo get_the_permalink(); ?>"><h2><?php the_title() ?></h2></a>
                          <p style="font-size:15px;"><?php echo the_excerpt_max_charlength(150); ?></p>
                          <!-- <a href="<?php echo esc_url( $url ); ?>"><div class="botao"><p>Leia Mais</p></div></a> -->
                        </div>
                        <div class="img-banner">
                          <?php the_post_thumbnail( ); ?>
                        </div>

                      <div class="slide-progress"></div>
                      </div>
                    <?php endwhile; ?>

            <?php }
            wp_reset_postdata();
            ?>

        </div>


            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev" style="background-image:none">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next" style="background-image:none">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>

      </div>

      <div class="row posts">

        <?php
         $args = array( 'post_type' => 'post', 'posts_per_page' => 4);
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

           <div class="post col-sm-4">
             <!-- <a href="<?php echo get_the_permalink(); ?>"> -->
                <?php the_post_thumbnail( ); ?>
              <!-- </a> -->
                <div class="excerpt">
                  <ul class="lista-categoria">
                    <?php
                    foreach((get_the_category()) as $category) {
                      echo '<li class="hashtags">' . $category->cat_name . '</li>';
                    }
                    ?>
                  </ul>
                  <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                  <p><?php the_excerpt(); ?></p>
                </div>

              <div class="mask-post col-sm-4">
                <a href="<?php echo get_the_permalink(); ?>">
                <div class="text">
                  <ul class="lista-categoria">
                    <?php
                    foreach((get_the_category()) as $category) {
                      echo '<li class="hashtags">' . $category->cat_name . '</li>';
                    }
                    ?>
                  </ul>
                  <h3><?php echo get_the_title(); ?></h3>
                  <button type="button" class="botao" name="button">Leia Mais</button>
                </div>
              </div>
           </div>

       <?php endwhile; // end of the loop. ?>
       <?php endif; ?>

      </div>

      <a href="http://sistran.tempsite.ws/index.php/blog/"><button type="button" class="botao" name="button">Ir para o blog</button></a>

    </div>
</div>
<!-- fim div5 -->

<!-- Div contato -->
<div class="div6">
  <div class="container">

    <div class="row" style="margin-bottom: 50px;">
      <div class="col-sm-6 col-sm-offset-3">
        <h1>Contato</h1>
        <hr class="titulo">
      </div>
    </div>

    <div class="row">
      <div class="col-sm-3 col-sm-offset-2">
        <div class="row padding">
          <h4>Sistran São Paulo:</h4>
          <p>Rua Luigi Galvani, 70 - 5º e 6º andares</p>
          <p>Brooklin Novo - CEP:04575-020</p>
        </div>
        <div class="row padding">
          <h4>Telefone:</h4>
          <p>+55 11 2192-4400</p>
        </div>
      </div>

      <div class="col-sm-6 contato">
        <?php echo do_shortcode('[contact-form-7 id="4" title="Contato"]'); ?>
      </div>
    </div>

  </div>
</div>
<!-- fim div6 -->
<?php get_footer(); ?>
