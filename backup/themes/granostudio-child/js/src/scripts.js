$(document).ready(function() {

// OWL CAROUSEL ===============================================
  $(".owl-carousel-0").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
  $(".grano-carousel-conteudo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
    $(".grano-carousel-conteudo-1").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
// /OWL CAROUSEL ==============================================

$(document).ready(function(){
  $(".slider-active").owlCarousel({
  loop:true,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
  });
});


// bxslider crousel ===============================================
  var owlHome = $('.owl-carousel');
  owlHome.owlCarousel({
    items:1,
    dots: false,
    loop:true,
    autoplay:true,
    autoplayHoverPause:true
  });

  owlHome.on('initialize.owl.carousel', function(event) {
    startProgressBar();
  });
  owlHome.on('initialized.owl.carousel', function(event) {
    startProgressBar();
  });
  owlHome.on('translate.owl.carousel', function(event) {
    resetProgressBar();
  });
  owlHome.on('translated.owl.carousel', function(event) {
    startProgressBar();
  });
  startProgressBar();

  function startProgressBar() {
    // apply keyframe animation
    $('.slide-progress').css({
      'width': '100%',
      'transition': 'width 5000ms'
    });
  }
  function resetProgressBar() {
    $('.slide-progress').css({
      'width': 0,
      'transition': 'width 0s'
    });
  }

// Back to Top ===============================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ==============================================


// Grano Scroll ANIMATION ====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 }

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ==================================

// / Banner home div4 ========================================
jQuery(document).ready(function($) {

      $('#myCarousel').carousel({
              interval: 10000
      });

      $('#carousel-text').html($('#slide-content-0').html());

      //Handles the carousel thumbnails
     $('[id^=carousel-selector-]').click( function(){
          var id = this.id.substr(this.id.lastIndexOf("-") + 1);
          var id = parseInt(id);
          $('#myCarousel').carousel(id);
      });

      // When the carousel slides, auto update the text
      $('#myCarousel').on('slid.bs.carousel', function (e) {
               var id = $('.item.active').data('slide-number');
              $('#carousel-text').html($('#slide-content-'+id).html());
      });
    });

// / Banner home div4 ========================================

});
