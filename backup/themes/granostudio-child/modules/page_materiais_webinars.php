<?php get_header(); ?>

<div class="titulo-materiais">
  <h1>Materiais educativos e Webinars</h1>
  <hr class="titulo">
  <p class="col-sm-8 col-sm-offset-2">Baixe os whitepapers, documentos e webinars que a Sistran Brasil preparou sobre tecnologia da informação e mercado Segurador.</p>
</div>

<div class="container posts-ebooks">
  <div class="line">
    <h3>Materiais educativos</h3>
    <hr>
    <div class="box-search">
        <form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <div class="form-group">
          <div class="input-group input-group-unstyled">
            <input type="text" class="form-control"	placeholder="Pesquisar..." id="inputGroup" name="s"/>
            <span class="input-group-addon">
              <i class="fa fa-search"></i>
            </span>
          </div>
        </div>
      </form>
    </div>

  </div>

  <div class="row posts">

    <?php
     $args = array( 'post_type' => 'e-book', 'posts_per_page' => 6);
     $loop = new WP_Query( $args );

     if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

       <div class="post col-sm-4">
         <!-- <a href="<?php echo get_the_permalink(); ?>"> -->
            <?php the_post_thumbnail( ); ?>
          <!-- </a> -->
            <div class="excerpt">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
              <p><?php the_excerpt(); ?></p>
            </div>

          <div class="mask-post col-sm-4">
            <a href="<?php echo get_the_permalink(); ?>">
            <div class="text">
              <ul class="lista-categoria">
                <?php
                foreach((get_the_category()) as $category) {
                  echo '<li class="hashtags">' . $category->cat_name . '</li>';
                }
                ?>
              </ul>
              <h3><?php echo get_the_title(); ?></h3>
              <button type="button" class="botao" name="button">Leia Mais</button>
            </div>
          </div>
       </div>

   <?php endwhile; // end of the loop. ?>
   <?php endif; ?>

  </div>

</div>

<div class="container">
  <div class="line">
    <h3>Webinars</h3>
    <hr class="hr-webinars">
  </div>

  <div class="row videos">

  <?php
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $args = array( 'post_type' => 'videos', 'posts_per_page' => 6, 'paged' => $paged, 'page' => $paged);
    $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post();
        echo '<div class="col-sm-6 posts-videos">';

        $video = get_post_meta( get_the_ID(), '_url', 1 );

        ?>
        <a href="<?php echo 'https://www.youtube.com/watch?v=' . substr($video, strpos($video,'?v=')+strlen('?v=')); ?>">
          <h3>
            <?php echo get_the_title();?>
          </h3>
        </a>

        <a href="<?php echo 'https://www.youtube.com/watch?v=' . substr($video, strpos($video,'?v=')+strlen('?v=')); ?>">
          <img src="<?php echo 'http://img.youtube.com/vi/' . substr($video, strpos($video,'?v=')+strlen('?v=')) . '/0.jpg'; ?>" width="100%">
        </a>

      <?php
      echo '</div>';

      endwhile;
  ?>


  </div>

  <div class="row visite-blog">
    <button type="button" class="botao botao-home" name="button">Conheça também nosso blog</button>
  </div>


</div>

<?php get_footer(); ?>
