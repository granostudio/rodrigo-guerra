<?php get_header(); ?>

<!-- Div1 -->
<div class="solucoes">
	<h1>Soluções de Negócios</h1>
    <hr class="titulo" style="width: 18%;">	
    <p class="paragrafos-ini">Veja como a Sistran pode ajudar sua Seguradora nos mais variados desafios de negócios.</p>

    <div class="container">
      <div class="div1-solucoes row">
         <p class="paragrafos-ini">Podemos oferecer nossos produtos e serviços através dos seguintes modelos</p>  
	      <div class="col-sm-3">
			<img src="">
			<p>Consultoria</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>

	      <div class="col-sm-3">
			<img src="">
			<p>Projetos</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>

	      <div class="col-sm-3">
			<img src="">
			<p>Body Shop</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>

	      <div class="col-sm-3">
			<img src="">
			<p>Outsourcing</p>
			<p>loren ipsum dolor sit ament adispicing consectetur</p>		      	
	      </div>
      </div>
    </div>


</div>

<div class="solucoes-div2">
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1">
			<h3>Por que muitos projetos de tecnologia em Seguradoras falham?</h3>
			<p>São vários os motivos: os mais comuns são falta de especialização dos fornecedores de TI no mercado de Seguros, que possui uma dinâmica própria e com altos níveis de regulação. Por esse mesmo motivo, produtos trazidos de fora e não adaptados à realidade local também se mostram incapazes de resolver problemas e necessidades nacionais.</p>

			<h3>Você procura uma solução de negócios para Seguradora com bom equilíbrio custo-benefício?</h3>
			<p>A resposta a essa questão são ofertas que possuam padrão internacional, metodologia comprovada e preço local. <a href="">Saiba mais em "Sobre a Sistran."</a></p>
		</div>
	</div>
</div>

<div class="solucoes-div3">
	<div class="container">
		<h1>Parceiros</h1>
    	<hr class="titulo" style="width: 10%;">	

    	<div class="col-sm-10 col-sm-offset-1">
    		<div class="col-sm-4"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice1@2x.png"></div>
    		<div class="col-sm-4"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice2@2x.png"></div>
    		<div class="col-sm-4"><img src="<?php echo get_stylesheet_directory_uri();?>/img/slice3@2x.png"></div>
    	</div>
	</div>
</div>

<div class="solucoes-div4">
	<h1>Soluções</h1>
    <hr class="titulo" style="width: 10%;">	

    <div class="col-sm-4">
    	<p>SISE RC</p>
    	<p>Gestão Produtos - Responsabilizade Civil</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE RD</p>
    	<p>Gestão Produtos - Risco Diversos</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE RE (P&C ERP)</p>
    	<p>Gestão Produtos para Ramos Elementares</p>
    </div>

    <div class="col-sm-4">
    	<p>RESSEGUROS</p>
    	<p>Gestão dos Contatos de Resseguros</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE CONNECT</p>
    	<p>Plataforma de vendas que compreende todo o ciclo do negócio, da cotação até a proposta</p>
    </div>
    <div class="col-sm-4">
    	<p>SISE VIDA</p>
    	<p>Solução global integrada que permite gerenciamento completo de produtos de Seguro de Vida</p>
    </div>

</div>

<?php get_footer(); ?>