<?php

/**
* Módulo:
* ***** Equipe - Page Template *****
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

function module_equipe(){

        echo '<div class="container"><div class="row">';
        $blogusers = get_users();
        // Array of WP_User objects.
        // print_r($blogusers);
        foreach ( $blogusers as $user => $value ) {
          ?>
              <div class="col-sm-4">
                <?php
                // has photo user
                $user_img_id = get_user_meta($value->data->ID, "_user_foto_id", true);
                if( !empty($user_img_id)){
                  $user_img_url = wp_get_attachment_image_src($user_img_id,"medium");
                  echo '<img src="'.$user_img_url[0].'" alt="" class="userfoto img-responsive">';
                }else{
                  echo '<img src="'.get_template_directory_uri().'/img/default/carousel.gif" alt="" class="userfoto img-responsive">';
                }
                 ?>

                <h1><?php echo $value->data->user_nicename; ?></h1>
                <p><?php  echo get_user_meta($value->data->ID, 'description', true ); ?></p>
              </div>
          <?php
        }
        echo "</div></div>";
}
 ?>
