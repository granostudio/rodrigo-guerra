// // PLUGIN GRANO SCROLL ANIMATION
// // AUTHOR GRANO STUDIO
// // 2016
var objs          = new Array(),
    $document     = jQuery(document),
    $window       = jQuery(window);



function check_if_in_view() {
  var target        = jQuery('.scroll-pointer');

  if(jQuery('.scroll-ani').length){
    post = jQuery('.scroll-ani');
  }

  var targettop = target.offset().top;
  var window_height = $window.height();
  var window_top_position = $window.scrollTop();
  var window_bottom_position = (window_top_position + window_height);


  if (typeof post != 'undefined'){
    $.each(post, function() {
      var post = $(this);
      var element_top_position = $(this).offset().top;
      var anitype = $(this).attr('data-anitype');

      var element_height = post.outerHeight();
      var element_top_position = post.offset().top;
      var element_bottom_position = (element_top_position + element_height);

      // debug
       target.text(targettop);

      if ((element_bottom_position >= window_top_position) &&
          (element_top_position <= window_bottom_position)) {

        // if(!post.hasClass('aniok')){
          post.addClass('aniok');
          post.addClass('animated '+anitype);
        // }

      } else {
        post.removeClass('animated'+anitype);
      }
    });
  }

}

function construc(){
  $document.on('scroll resize', check_if_in_view);
  $document.trigger('scroll');
}


function GranoScrollAni(obj){
  // criar array objs para add a class .scroll-ani
  jQuery.each(obj, function(key, value) {
    if ( $(key).length ) {
      $(key).addClass('scroll-ani');
      $(key).attr('data-anitype', value);
    }
  });

  construc();

}
