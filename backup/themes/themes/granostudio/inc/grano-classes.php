<?php
/**
 * Grano Site Classes Uteis
 *
 * @author Grano studio
 */


 /**
  * Verifica se quais tipos de conteudo foram ativados
  * obs: ele retorna como uma array
  */

class modelosCheck
{
  public function ativos(){
    $custom_post_type = get_option('custom_post_type');
    $ativos = $custom_post_type['posttype_opt'];
    return $ativos;
  }
}


/**
 * Função para pegar theme options
 */

function grano_get_options($page,$campo){
   $valor = get_option($page);
   return $valor[$campo];
}
