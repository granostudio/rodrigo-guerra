<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<!-- btn voltar -->
  <div class="btnvoltar-single">
    <a href="<?php echo get_home_url(); ?>" class="btn"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
  </div>
<!-- /btn voltar -->

<!-- Page Content -->
    <div class="container blog-single">

        <div class="row">

            <!-- Blog Post Content Column -->
            <div class="<?php echo is_active_sidebar( 'sidebar_blog' ) ? 'col-sm-8' : 'col-sm-8 col-sm-offset-2'; ?>">

                <!-- Blog Post -->

								<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                <!-- Title -->
                <h1><?php echo get_the_title(); ?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author(); ?></a>
                </p>

                <hr>

                <!-- Date/Time -->
								<?php $data_atualização = get_the_modified_date(); ?>
                <p><span class="fa fa-clock-o"></span> <?php echo get_the_date(); ?> | <?php echo !empty($data_atualização) ? "Atualizado dia ".$data_atualização : ''; ?></p>

                <hr>

								<?php
								// post thumbanil
								if (has_post_thumbnail()) {
										the_post_thumbnail('large', array( 'class' => 'img-responsive' ));
								}
								 ?>

                <!-- Preview Image -->


                <hr>

                <!-- Post Content -->
                <?php the_content(); ?>

                <hr>

                <!-- Blog Comments -->


            </div>
					<?php endwhile; // end of the loop. ?>

            <!-- Blog Sidebar Widgets Column -->
            <?php get_sidebar(); ?>


        </div>
        <!-- /.row -->


    </div>
    <!-- /.container -->

<?php get_footer(); ?>
