<?php
/**
 * The template used for displaying result search
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-sm-12'); ?>>
	<h2>
			<a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title();?></a>
	</h2>


	<?php

	if ( has_post_thumbnail() ) {

		$thumb_id = get_post_thumbnail_id(get_the_ID());
		$thumb_url = wp_get_attachment_image_src($thumb_id, 'large');
		echo '<div class="single-post-thumb" style="background-image: url('.$thumb_url[0].')"></div>';

	 }
	 ?>
 <?php the_excerpt(); ?>
 <a class="btn btn-primary" href="#">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

 <hr>

</article><!-- #post-## -->
