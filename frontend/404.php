<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<?php
if ( is_404() ) { ?>

<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>

	<div id="primary" class="container" style="margin-bottom: 50px;">
		<div id="content" class="site-content" role="main" style="margin-top: 140px;">
			
			<div class="col-sm-8">
				<header class="page-header" style="border: none;margin: 50px 0 20px;">
					<h1 class="page-title" style="color: #0D2D25;">Erro 404!</h1>
				</header>
				<div class="page-content">
					<p style="color: #0D2D25;"><strong>Parece que a página que você buscou não existe neste servidor!</strong></p>
					<p>O que pode ter acontecido:</p>
					<ul>
						<li>A página foi movida ou deletada.</li>
						<li>O link usado pode ser antigo ou pode estar corrompido.</li>
						<li>O endereço URL pode ter sido digitado incorretamente.</li>
					</ul>
					<p>Sugerimos que você volte à nossa home clicando <a href="<?php echo home_url(); ?>" style="text-decoration: underline;color: #0D2D25;">aqui</a> ou pesquise o que deseja encontrar!</p>	
				</div>
			</div>

			<div class="col-sm-4">

				<img src="<?php echo get_stylesheet_directory_uri();?>/img/cabeca.png" class="img-404">
				
				<form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform">
	                <div class="form-group">
	                    <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
	                    <label for="search-field">
	                        <i class="fa fa-search"></i>
	                    </label>
	                </div>
	            </form>

			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>