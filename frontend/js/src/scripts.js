$(document).ready(function() {

// OWL CAROUSEL ===============================================
  $(".owl-carousel-0").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
  $(".grano-carousel-conteudo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,

  });
    $(".grano-carousel-conteudo-1").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 100,
      paginationSpeed : 100,
      singleItem:true,
      loop:true,
      autoplay:true,
      nav: true

  });
// /OWL CAROUSEL ==============================================

$(document).ready(function(){
  $(".slider-active").owlCarousel({
  loop:true,
  nav:true,
  responsive:{
      0:{
          items:1
      },
      600:{
          items:1
      },
      1000:{
          items:1
      }
  }
  });
});


// bxslider crousel ===========================================
  var owlHome = $('.owl-carousel');
  owlHome.owlCarousel({
    items:1,
    dots: false,
    loop:true,
    autoplay:true,
    autoplayHoverPause:true
  });


// Back to Top ================================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================


// /Menu transition ===========================================


$(document).ready(function () {
    (function($) {                            
        $(window).scroll(function(){                          
            if ($(this).scrollTop() > 500) {
                $('.navbar').css({"margin-top": "0px"});
            } 
            else {
                $('.navbar').css({"margin-top": "-91px"});
            }
        });
  })(jQuery);
}); 


// /Menu transition ===========================================






// Grano Scroll ANIMATION =====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 }

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ===================================


$(document).ready(function(){
  AOS.init({ disable: 'mobile' });
}); 

}); 


       