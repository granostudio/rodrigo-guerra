<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// environments config
$host = $_SERVER['HTTP_HOST'];
$hosturi = dirname($_SERVER["SCRIPT_NAME"]);
// echo dirname($hosturi);
// echo $host;

if ($hosturi == '/' || $hosturi == '/dev' || $hosturi == "\\"){
	$json = file_get_contents('ambientconfig.json');

} elseif (strpos($_SERVER['REQUEST_URI'],'/wp-admin/') !== FALSE) {
	$json = file_get_contents('../ambientconfig.json');

} else {
	$json = file_get_contents('ambientconfig.json');
}

$datasite = json_decode($json);
$urllive = $datasite->clienteurl;
$urldev = $datasite->clienteurl;
 // echo $hosturi;

 if($host == "localhost:9001"){
 	// localhost
 	//

			$prefixo = "live_";
 			$db_name = $datasite->clientenome;
 			$db_user = $datasite->databasedevuser;
 			$db_pass = $datasite->databaselivepass;
 			$db_host = $datasite->databasedevurl;
 			$wp_ulr  = 'localhost:9001/';
 }elseif ($host == $urldev) {
 	// developers
			$prefixo = "live_";
 			$db_name = $datasite->clientenome;
 			$db_user = $datasite->databasedevuser;
 			$db_pass = $datasite->databaselivepass;
 			$db_host = $datasite->databasedevurl;
 			$wp_ulr  = $urldev;
 }else {
 	// live
			$prefixo = "live_";
 			$db_name = $datasite->clientenome;
 			$db_user = $datasite->databaseliveuser;
 			$db_pass = $datasite->databaselivepass;
 			$db_host = $datasite->databaseliveurl;
 			$wp_ulr  = $urllive;

 }

// switch($host) {
// 	case 'localhost:9001';
// 		// localhost
//
// 		$db_name = $datasite->clientenome.'dev';
// 		$db_user = $datasite->databasedevuser;
// 		$db_pass = $datasite->databaselivepass;
// 		$db_host = $datasite->databasedevurl;
// 		$wp_ulr  = 'localhost:9001/';
//
// 		break;
// 	case $urllive:
// 		// live
// 		$db_name = $datasite->clientenome.'live';
// 		$db_user = $datasite->databaseliveuser;
// 		$db_pass = $datasite->databaselivepass;
// 		$db_host = $datasite->databaseliveurl;
// 		$wp_ulr  = $urllive;
//
// 		break;
// 	case $urldev:
// 		// developers
// 		$db_name = $datasite->clientenome.'dev';
// 		$db_user = $datasite->databasedevuser;
// 		$db_pass = $datasite->databaselivepass;
// 		$db_host = $datasite->databasedevurl;
// 		$wp_ulr  = $urldev;
//
// 		break;
//
// }

//reparar caso o banco não tenha sido instalado em algum ambiente
define('WP_ALLOW_REPAIR', true);



// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', $db_name);

/** MySQL database username */
define('DB_USER', $db_user);

/** MySQL database password */
define('DB_PASSWORD', $db_pass);

/** MySQL hostname */
define('DB_HOST', $db_host);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('WP_HOME', 'http://'.$wp_ulr);
define('WP_SITEURL','http://'.$wp_ulr);

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '+D;)|4E}-7*#vxH`S22zhy)R-FT|bo|+&Bm!;6[U6~-=KDbAKUOd-J!o(yB+9eJM');
 define('SECURE_AUTH_KEY',  '(r;Vsu8dSS[EMJn|ioQ+lC:9hS%2AMYl+{f_4LH-Nv3161#6+LU(w_L4Rq#g3L v');
 define('LOGGED_IN_KEY',    'C&=@p1x;}_+-Iso]+H+k|3-$Tq&` WKpk2Hk!=<(SS<0/#p3Q$N>C-qo9nnpbj z');
 define('NONCE_KEY',        ',?n|kEtjy!J|_d^1i#2?FCQ5unEr7+1@n-|v_FFe5}j.L4; ]t,J.(cl+Oo[cR+*');
 define('AUTH_SALT',        'Uk7]DlwF~|2Z.F9qRi2H_#!1+Pn*-n6ZE8|s:<2z&ZZ//)U4,zdA/sdL0@c`KzLw');
 define('SECURE_AUTH_SALT', 'Nf;&Eb$|Wr&4RWMn /hq$T1e&nfm<y2]r9@R2j=,wBONvIU6Nwk!K_xX)S7x|dVL');
 define('LOGGED_IN_SALT',   '+a+5Dv..seE/b;B,C.i05fBCR5ZG>+,4M[<{QtZe.lBpJW$oN_:Y`Q/`SA0q.%mQ');
 define('NONCE_SALT',       ']Bg)Pgt@-x7i+QGe|tdMI8IjhGc]iJ5$83_y7/|i.(sbylC}ffIB|O6Y,UOE*PhV');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = $prefixo;

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);
define ( 'AUTOMATIC_UPDATER_DISABLED' , false);




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

// default theme granoexpresso
define( 'WP_DEFAULT_THEME', 'granostudio-child' );

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
