(function($) {
  var optModelo = $("select[id*=page_modulo]");
  var btnRow = $('.cmb-shift-rows');

  // update optModelo
  jQuery.fn.update = function(){
    var newElements = $(this.selector),i;
    for(i=0;i<newElements.length;i++){
      this[i] = newElements[i];
    }
    for(;i<this.length;i++){
      this[i] = undefined;
    }
    this.length = newElements.length;
    return this;
  };

  function removeDisplay(obj){
    var modulo = obj.closest('.cmb-field-list');
    //remove todas as class display se existem
    modulo.find('.cmb-row').each(function(){
      if($(this).hasClass('display')){
         $(this).removeClass('display')
      }
    });
  }

  function change(){
    optModelo.on("change", function() {
        removeDisplay($(this));
        switch($(this).val()){
          case "Banner":
            $(this).closest('.cmb-field-list').find('.cmb-type-file-list').addClass('display');
          break;
          case "Contato":
            $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').addClass('display');
          break;
          case "Externo":
            $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').addClass('display');
          break;
          case "Bannerconteudo":
            $(this).closest('.cmb-field-list').find('.cmb-type-post-search-text').addClass('display');
          break;
        }
      });
  }




  // quando add novo modluo
  $('.cmb-add-group-row').on("click", function(evt){

    jQuery.fn.update = function(){
      var newElements = $(this.selector),i;
      for(i=0;i<newElements.length;i++){
        this[i] = newElements[i];
        console.log(this[i]);
      }
      for(;i<this.length;i++){
        this[i] = undefined;
        onsole.log(this[i]);
      }
      this.length = newElements.length;
      return this;
    };

    evt.preventDefault();
    var t = 0;

    var interval = window.setInterval(function(){
        console.log('má oiiiii!');
        optModelo.update();
        change();
        t++;
        if(t==5){
          clearInterval(interval);
        };
    },1);
  });
  //quando muda de posicão
  $(document).on('DOMNodeInserted', function() {
    $('.cmb-shift-rows').on("click", function(){
      var t = 0;
      var interval = window.setInterval(function(){
          optModelo.trigger("change");
          t++;
          if(t==5){
            clearInterval(interval);
          };
      },1);
    });

  });

  // funcao init
  function initModulos(optModelo){
    removeDisplay($(this));
    optModelo.each(function(){
      switch(  $(this).find('option:selected').val()){
        case "Banner":
          $(this).closest('.cmb-field-list').find('.cmb-type-file-list').addClass('display');
        break;
        case "Contato":
          $(this).closest('.cmb-field-list').find('.cmb-type-select[class*=formcontato]').addClass('display');
        break;
        case "Externo":
          $(this).closest('.cmb-field-list').find('.cmb-type-text-medium[class*=modeloexterno]').addClass('display');
        break;
        case "Bannerconteudo":
          $(this).closest('.cmb-field-list').find('.cmb-type-post-search-text').addClass('display');
        break;
      }
    });
  }
  initModulos(optModelo);
  change();



 })(jQuery);
