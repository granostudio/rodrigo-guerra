


$(document).ready(function() {


// nav home
  var navBar     = $('.navbar'),
      body       = $('body'),
      scrollBody = 0;
  if(navBar.hasClass('navhome')){
    $(window, document, 'body').scroll(function(event) {
      scrollBody = body.scrollTop();
      if(scrollBody>=111){
        navBar.addClass('active');
      } else if (scrollBody<111) {
        navBar.removeClass('active');
      }
    });

  }
// / nav home


// bxslider crousel ===============================================
  var owlHome = $('.owl-carousel');
  owlHome.owlCarousel({
    items:1,
    dots: false,
    loop:true,
    autoplay:true,
    autoplayHoverPause:true
  });

  owlHome.on('initialize.owl.carousel', function(event) {
    startProgressBar();
  });
  owlHome.on('initialized.owl.carousel', function(event) {
    startProgressBar();
  });
  owlHome.on('translate.owl.carousel', function(event) {
    resetProgressBar();
  });
  owlHome.on('translated.owl.carousel', function(event) {
    startProgressBar();
  });
  startProgressBar();

  function startProgressBar() {
    // apply keyframe animation
    $('.slide-progress').css({
      'width': '100%',
      'transition': 'width 5000ms'
    });
  }
  function resetProgressBar() {
    $('.slide-progress').css({
      'width': 0,
      'transition': 'width 0s'
    });
  }
// /OWL CAROUSEL ===============================================

// Back to Top ===============================================
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    $('#back-to-top').tooltip('show');
// /Back to Top ===============================================


// Grano Scroll ANIMATION ====================================
obj = {
  '.textocombotao-1 .row .texto' : 'fadeIn',
  '.textocombotao-2 .row .texto' : 'fadeIn',
  '.textocombotao-3 .row .texto' : 'fadeInUp'
 }

GranoScrollAni(obj);

// / Grano Scroll ANIMATION ==================================

// Search ====================================

$('.buscar').on('click', function(event) {
  event.preventDefault();
  /* Act on the event */
  $('.box-buscar').addClass('active');
  $('.box-buscar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',
    function(e) {
      $('.campo-buscar').focus();
  });
});
$('.box-buscar .btnFechar').on('click', function(event) {
  event.preventDefault();
  /* Act on the event */
  $('.box-buscar').removeClass('active');
});
$('.box-buscar .btn').on('click', function(event) {
  // event.preventDefault();
  /* Act on the event */
  $('.box-buscar').removeClass('active');
});
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
      $('.box-buscar').removeClass('active');
    } else{


    }
});


// Toggle ======================================

$("#flip5").click(function(){
    $("#panel5").slideToggle("slow");
});
$("#flip6").click(function(){
  $("#panel6").slideToggle("slow");
});

// / Search ====================================


// / FAQ PROAMA ================================

    $('#back-to-div5').on('click', function(){
        $('html, body').animate({
            scrollTop: $('.div5').offset().top -120
        }, 500);
      });
      $('#back-to-div6').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div6').offset().top -120
          }, 500);
        });
      $('#back-to-div7').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div7').offset().top -120
          }, 500);
        });
      $('#back-to-div8').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div8').offset().top -120
          }, 500);
        });
      $('#back-to-div9').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div9').offset().top -120
          }, 500);
        });
      $('#back-to-div11').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div11').offset().top -120
          }, 500);
        });
      $('#back-to-div12').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div12').offset().top -120
          }, 500);
        });
      $('#back-to-div14').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div14').offset().top -120
          }, 500);
        });
      $('#back-to-div15').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div15').offset().top -120
          }, 500);
        });
      $('#back-to-div17').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div17').offset().top -120
          }, 500);
        });
      $('#back-to-div18').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div18').offset().top -120
          }, 500);
        });
      $('#back-to-div19').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div19').offset().top -120
          }, 500);
        });
      $('#back-to-div20').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div20').offset().top -120
          }, 500);
        });
      $('#back-to-div21').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div21').offset().top -120
          }, 500);
        });
      $('#back-to-div22').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div22').offset().top -120
          }, 500);
        });
      $('#back-to-div23').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div23').offset().top -120
         }, 500);
        });
      $('#back-to-div24').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div24').offset().top -120
          }, 500);
        });
      $('#back-to-div25').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div25').offset().top -120
          }, 500);
        });
      $('#back-to-div26').on('click', function(){
          $('html, body').animate({
              scrollTop: $('.div26').offset().top -120
          }, 500);
        });
      $('#back-to-div27').on('click', function(){
         $('html, body').animate({
              scrollTop: $('.div27').offset().top -120
          }, 500);
        });
        $('#back-to-div28').on('click', function(){
            $('html, body').animate({
                scrollTop: $('.div28').offset().top -120
            }, 500);
          });

// / Back-to-top FAQ

$(window).scroll(function () {
    if ($(this).scrollTop() > 50) {
        $('#back-to-topfaq').fadeIn();
    } else {
        $('#back-to-topfaq').fadeOut();
    }
});
// scroll body to 0px on click
$('#back-to-topfaq').click(function () {
    $('#back-to-topfaq').tooltip('hide');
    $('body,html').animate({
        scrollTop: 900
    }, 800);
    return false;
});

$('#back-to-topfaq').tooltip('show');

$('#newslettler').on('click', function(){
    $('html, body').animate({
        scrollTop: $('.cadastrar').offset().top -120
    }, 500);
  });


});
