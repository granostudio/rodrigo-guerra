<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<div class="tituloI titulo titulo2">
  <i class="fa fa-linkedin fa-7x" aria-hidden="true"></i>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-12 junte-se">
      <h1>Junte-se ao nosso grupo do LinkedIn</h1>
      <p>Se você é executivo, gestor, profissional, estudioso, estudante e/ou observador das questões que envolvem a evolução dos processos e maturidade de gestão em hospitais, gostaria de convidá-lo a participar do grupo “Maturidade de Gestão Hospitalar” no LinkedIn.</p>
      <p>Esse é um um espaço para debater, compartilhar experiências e trocar conhecimentos sobre governança corporativa, estratégia empresarial, gestão de processos, tecnologia, gestão de pessoas.</p>
    </div>
  </div>
</div>

<div class="div_conteudo" style="margin-top:-55px;">
  <div class="row_conteudo">
      <div class="coluna3">
        <div class="container">
          <div class="texto_linkedin">
            <p>
              Entendemos que a maturidade de gestão hospitalar é muito mais que estar informatizado, é a capacidade de alcançar resultados de forma contínua e consistente,
              por meio de pessoas capacitadas e metodologias comprovadas. Com isso, é possível utilizar a tecnologia com sólidos processos na execução da estratégia, aliados a um sistema de governança capaz de permitir o acompanhamento da organização e das ações planejadas.
            </p>
            <p style="padding-top:30px;font-size:17px;"><strong>Venha participar conosco dessa contrução!</strong></p>
            <a href="https://lnkd.in/eW8K8hx" class="btn btn-primmary" target="_blank">Inscreva-se</a>
          </div>
        </div>
      </div>
      <div class="coluna-direita_linkedin col2 hidden-xs">
        <div class="fundo_linkedin"></div>
      </div>
  </div>
</div>

<?php get_footer(); ?>
