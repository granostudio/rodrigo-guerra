<div class="tituloI">
    <h1>Quem Somos?</h1>
    <!-- <p class="texto">Moldando o futuro com as suas mãos</p> -->
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-7">
      <p>Em busca de competitividade, uma enxurrada de hospitais dos mais variados portes recorreu, a partir de meados dos anos 2000, a softwares de gest&atilde;o. Informatizar sistemas, processos administrativos e cl&iacute;nicos, contudo, representa apenas um passo no caminho rumo &agrave; <strong>maturidade de gest&atilde;o hospitalar</strong>, que demanda, al&eacute;m de <strong>tecnologias de gest&atilde;o</strong>, metodologias estruturadas de <strong>estrat&eacute;gia empresarial, governança corporativa, gerenciamento de processos de neg&oacute;cios </strong>e<strong> gest&atilde;o de pessoas.</strong></p>
      <p>Esse entendimento ficou claro para <strong>Roberto Gordilho</strong> ap&oacute;s <strong>coordenar </strong>a implantação de sistemas de gestão em mais de <strong>300 hospitais </strong>de todos os portes (pequeno, médio e grande), tipos (públicos, privados e filantrópicos) e que prestam serviços ao SUS e aos convênios e seguradoras.</p>
      <p>Com o objetivo de ajudar as organiza&ccedil;&otilde;es a <strong>irem al&eacute;m da tecnologia e informatiza&ccedil;&atilde;o</strong> e a elevarem seus n&iacute;veis de<strong> maturidade de gest&atilde;o hospitalar</strong> a patamares internacionais, Gordilho juntou seus 30 anos de experi&ecirc;ncia na &aacute;rea de Sa&uacute;de a metodologias acad&ecirc;micas e pr&oacute;prias para criar a <strong>GesSa&uacute;de</strong>.</p>
      <p>Apesar de nova, a GesSa&uacute;de j&aacute; nasce com s&oacute;lida experi&ecirc;ncia de TI em Sa&uacute;de, posicionando-se como uma consultoria especializada em fomentar a maturidade da gest&atilde;o em institui&ccedil;&otilde;es de Sa&uacute;de do Brasil. Para isso, atua com dois focos distintos: com <strong>consultoria individual para hospitais de grande porte</strong>, e com o <strong>Programa de Acelera&ccedil;&atilde;o da Maturidade de Gest&atilde;o da Sa&uacute;de (PROAMA), voltado a institui&ccedil;&otilde;es de Sa&uacute;de de pequeno e m&eacute;dio portes</strong>. O PROAMA age como um transformador da gest&atilde;o: tem dura&ccedil;&atilde;o de 12 meses e conta com 600 horas de atividades, entre workshops, capacita&ccedil;&otilde;es e <em>mentoring</em>. Podem participar hospitais, cl&iacute;nicas e centros de diagn&oacute;stico que n&atilde;o sejam concorrentes diretos em uma mesma regi&atilde;o.</p>
        <div class="row ">
          <div class="col-md-6">
            <div class="botao1" >
                <p>Saiba mais sobre os <br><a href="/servicos/">serviços de consultoria da GesSaúde</a></p>
            </div>
          </div>

          <div class="col-md-6">
            <div class="botao2" >
                <p>Saiba mais sobre o <br><a href="/proama/">Programa de Aceleração da <br>Maturidade de Gestão da Saúde (PROAMA)</a></p>
            </div>
          </div>

        </div>
        <div class="row">
          <div class="col-sm-4 col-sm-offset-4">
            <div class="cronograma-link">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/proama-download.png"><br/>
                <a href="http://www.gessaude.com.br/wp-content/uploads/2017/06/FolderDigitalGesSaude.pdf" download class="btn btn-primmary" target="_blank">Baixe o folder digital</a>
            </div>
          </div>
        </div>

    </div>

    <div class="col-sm-5 roberto">
      <img src="<?php echo get_stylesheet_directory_uri();?>/img/Gordilho.jpg" class="gordilho hidden-md hidden-lg">
      <div class="rowR">
        <p class="tituloR"><strong>ROBERTO GORDILHO</strong></p>
        <p>Roberto Gordilho é mestrando em Administração e especializado em Sistemas de Informação, Engenharia de Software e
          Desenvolvimento Web e em Finanças, Contabilidade e Auditoria.
          Já fez cursos de extensão na Kellogg Business School, em Chicago,
          e na Universidade da California (University of California, UCI), ambas nos Estados Unidos.</p>
      </div>

      <div class="rowF">
        <div class="textoR2">
          <p class="textoroberto">Gordilho foi diretor das empresas EXE Sistemas e Extreme Tecnologia.Também atuou como diretor corporativo de serviços da MV
          –empresa líder em tecnologia de gestão para Saúde no Brasil–, onde, entre 2011 e 2016, coordenou  direta e
          indiretamente a implantação do sistema de gestão em mais de 300 hospitais de pequeno, médio e grande portes,
          públicos, privados e filantrópicos em várias partes do País.</p>
        </div>

        <div class="fotoroberto hidden-xs hidden-sm">
          <div class="foto">
              <img src="<?php echo get_stylesheet_directory_uri();?>/img/roberto.png">
          </div>
        </div>
      </div>

    </div>
  </div>


</div>

<div class="cadastrar">
  <p>Cadastre-se para ter acesso a conteúdos exclusivos</p>
</div>

<?php echo do_shortcode('[mc4wp_form id="282"]'); ?>
