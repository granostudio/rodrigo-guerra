  <?php
  /**
   * The template for displaying all pages.
   *
   * This is the template that displays all pages by default.
   * Please note that this is the WordPress construct of pages
   * and that other 'pages' on your WordPress site will use a
   * different template.
   *
   * @package WordPress
   * @subpackage Twenty_Eleven
   * @since Twenty Eleven 1.0
   */

  get_header(); ?>

  <div class="tituloI titulo">
    <h1>Canal GesSaúde</h1>
  </div>

  <div class="container">

    <div style="margin-bottom:35px;width:225px;">
      <h2 style="border-bottom: 3px solid #33BB9C;">ÚLTIMOS VÍDEOS</a></h2>
    </div>

  <div class="row">

    <?php
      $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
      $args = array( 'post_type' => 'movies', 'posts_per_page' => 12, 'paged' => $paged, 'page' => $paged);
      $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post();
          echo '<div class="col-sm-6 col-md-4 post2">';
          echo '<div class="post-border post-border2">';
          $video = get_post_meta( get_the_ID(), '_url', 1 );

          ?>
          <a href="<?php echo get_the_permalink(); ?>">
            <img src="<?php echo 'http://img.youtube.com/vi/' . substr($video, strpos($video,'?v=')+strlen('?v=')) . '/0.jpg'; ?>" width="100%">
          </a>

          <a href="<?php echo get_the_permalink(); ?>" style="color: #374E5A;">
            <div class="post-inner2">
              <h4>
                <?php echo get_the_title();?>
              </h4>
            <?php the_excerpt(); ?>
            </div>
          </a>

          <?php
        echo '</div>';
        echo '</div>';
        endwhile;
    ?>

  </div>

  <div class="">
    <ul class="pager">
      <li class="previous"><?php next_posts_link( '<i class="fa fa-chevron-left fa-lg" aria-hidden="true"></i> Vídeos Anteriores', $loop->max_num_pages); ?></li>
      <li class="next"><?php previous_posts_link( ' Vídeos Recentes <i class="fa fa-chevron-right fa-lg" aria-hidden="true"></i>', $loop->max_num_pages ); ?></li>
    </ul>
  </div>

  </div>

  <?php get_footer(); ?>
