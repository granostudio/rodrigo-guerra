<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

 

  <?php $args = array( 'post_type' => 'movies', 'posts_per_page' => 5 );
    $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post();
        echo '<div class="">';
        $video = get_post_meta( get_the_ID(), '_url', 1 );


        ?>
        <h4>
          <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title();?></a>
        </h4>
        <?php the_content(); ?>

        <iframe width="100%" height="250px"
        src="<?php echo 'https://www.youtube.com/embed/' . substr($video, strpos($video,'?v=')+strlen('?v=')); ?>"
        frameborder="0" allowfullscreen></iframe>

        </div>

        <?php
      endwhile;
  ?>

</div>
