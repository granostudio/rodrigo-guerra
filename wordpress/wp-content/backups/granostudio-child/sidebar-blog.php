<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<div class="col-sm-4">
  <h3 style="margin-top: 0px;margin-bottom: 16px;">ÚLTIMAS POSTAGENS</h3>  
</div>

<?php
       $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
       $args = array( 'post_type' => 'post', 'posts_per_page' => 3, 'paged' => $paged, 'page' => $paged);
       $loop = new WP_Query( $args );

       if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

       <div class="col-sm-4 post">

          <div class="social-share <?php if (has_post_thumbnail()){ echo 'thumb';}?>">
            Compartilhe
            <?php GranoSocialShare(get_the_permalink()) ?>
          </div>

          <div class="post-border">
            <?php if (has_post_thumbnail()){?>
              <?php $thumbUrl = get_the_post_thumbnail_url($post_id, 'medium' ); ?>
              <div class="img-thumb" style="background-image: url(<?php echo $thumbUrl; ?>)"></div>
            <div class="post-inner thumb">
            <?php } else {
            ?>
            <div class="post-inner">
            <?php

            };
              $categories = get_the_category( $post_id );
              $i=1;
              $cats = array();
              foreach( $categories as $category ) {
                $cat = get_category( $category );
                $cats = array( 'name' => $cat->name, 'slug' => $cat->slug, 'link'=> $cat->term_id );
                $catlink = get_category_link($cats['link']);
                if($i==count($categories)){
                  if ($cats['slug']!="sem-categoria") {
                    echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>";
                  }else{
                    echo "<a href='".$catlink."' class='categoria'></a>";
                  }
                } else {
                  if ($cats['slug']!="sem-categoria") {
                    echo "<a href='".$catlink."' class='categoria'>".$cats['name']."</a>".', ';
                  }else{
                    echo "<a href='".$catlink."' class='categoria'></a>";
                  }
                }
                $i++;
              }
              ?>

              <h4><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h4>

               <!-- Remove o jetpack do post-->
              <?php
              if ( function_exists( 'sharing_display' ) ) {
                remove_filter( 'the_content', 'sharing_display', 19 );
                remove_filter( 'the_excerpt', 'sharing_display', 19 );
              }
              the_excerpt(); ?>
              <!---  --->
                <?php  ?>
              <!-- <a class="btn-default btn" href="<?php //echo get_the_permalink(); ?>">Leia mais</a> -->
            </div>
        </div>

        </div>

      <?php endwhile; ?>
      <?php endif; ?>

    </div>
