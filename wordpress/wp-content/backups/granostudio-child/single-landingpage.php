<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<?php $args = array( 'post_type' => 'Landing Page');
$loop = new WP_Query( $args );
  while ( $loop->have_posts() ) : $loop->the_post(); ?>

<div class="tituloI titulo">
  <h1>E-Book</h1>
</div>

<div class="container">
  <div class="row">
    <div class="col-sm-5 col-sm-offset-1">
      <div class="texto_conteudo">
        <h2 style="text-transform: inherit;margin-bottom:25px;"><?php echo get_the_title(); ?></h2>
        <p><?php the_content(); ?></p>

        <h3 style="margin-bottom: 35px;">Cadastre-se para ter acesso ao E-Book</h3>
          <?php echo do_shortcode('[contact-form-7 id="483" title="E-Book"]'); ?>
        </div>

    </div>

    <div class="col-sm-6">
      <?php if ( has_post_thumbnail() ) {
      	 the_post_thumbnail('post-thumbnail', array('class' => "img_ebook"));
       } else {
      	 echo '<img class="img_ebook" src="'.get_stylesheet_directory_uri().'/img/e-book.png"/>';
      } ?>
    </div>

  </div>
</div>



<?php endwhile; // end of the loop. ?>


<?php get_footer(); ?>


<!-- Contact Form Redirect -->
<?php

      // echo "<script>";
      // echo "var url = '".$arquivourl."';";
      //
      //
      // echo "document.addEventListener( 'wpcf7mailsent', function( event ) {";
      // echo "location = arquivoURL;";
      // echo "}, false );";
      // echo "</script>";
?>
