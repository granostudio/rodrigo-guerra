<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

<div class="container">

    <?php $video = get_post_meta( get_the_ID(), '_url', 1 ); ?>

    <iframe class="video_single"
    src="<?php echo 'https://www.youtube.com/embed/' . substr($video, strpos($video,'?v=')+strlen('?v=')); ?>"
    frameborder="0" allowfullscreen>
    </iframe>

    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div class="titulo_single">
      <h1>
        <?php echo get_the_title();?>
      </h1>
      <p>
        <?php the_content(); ?>
      </p>

      <script src="https://apis.google.com/js/platform.js"></script>
      <div class="g-ytsubscribe" data-channelid="UC7LzsZRRkyODPh4D2iMK_Lw" data-layout="default" data-count="hidden"></div>
    </div>

    <?php endwhile; // end of the loop. ?>

  <div style="margin-bottom:35px;width:225px;">
    <h2 style="border-bottom: 3px solid #33BB9C;"><a href="http://www.gessaude.com.br/videos/" style="color: #374E5A;">ÚLTIMOS VÍDEOS</a></h2>
  </div>

<div class="row">

  <?php $args = array( 'post_type' => 'movies', 'posts_per_page' => 6 );
    $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post();
        echo '<div class="col-sm-6 col-md-4 post2">';
        echo '<div class="post-border post-border2">';
        $video = get_post_meta( get_the_ID(), '_url', 1 );

        ?>
        <a href="<?php echo get_the_permalink(); ?>">
          <img src="<?php echo 'http://img.youtube.com/vi/' . substr($video, strpos($video,'?v=')+strlen('?v=')) . '/0.jpg'; ?>" width="100%">
        </a>
        <a href="<?php echo get_the_permalink(); ?>" style="color: #374E5A;">
          <div class="post-inner2">
            <h4>
              <?php echo get_the_title();?>
            </h4>
          <?php the_excerpt(); ?>
          </div>
        </a>

        <?php
      echo '</div>';
      echo '</div></a>';
      endwhile;
  ?>

</div>

<a href="http://www.gessaude.com.br/videos/" class="btn btn-primmary">Voltar</a>

</div>
<?php get_footer(); ?>
