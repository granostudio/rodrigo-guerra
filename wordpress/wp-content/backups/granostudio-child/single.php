<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=1932839393611611";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- btn voltar -->
  <div class="btnvoltar-single">
    <a href="<?php echo get_home_url(); ?>" class="btn"><i class="fa fa-angle-left" aria-hidden="true"></i> Voltar</a>
  </div>
<!-- /btn voltar -->


<!-- Page Content -->
    <!-- Thumb -->

    <?php if (has_post_thumbnail()){?>
      <?php $thumbUrl = get_the_post_thumbnail_url(get_the_ID(), 'large' ); ?>
      <div class="thumb-post" style="background-image: url(<?php echo $thumbUrl; ?>)">
        <h1 class="col-sm-8 col-sm-offset-2 title_single"><?php echo get_the_title(); ?></h1>        
      </div>
      <div class="container blog-single thumb-active">
      <!-- Title -->

    <?php } else {
      ?>
      <div class="container blog-single">
      <?php
    }?>
    <!-- / Thumb -->


        <div class="row">


            <!-- Blog Post Content Column -->
            <div class="<?php echo is_active_sidebar( 'sidebar_blog' ) ? 'col-sm-8' : 'col-sm-8'; ?>">

                <!-- Blog Post -->

								<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                <!-- Posicionando o jetpack em uma posição específica -->
                <?php
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                    $custom_likes = new Jetpack_Likes;
                    echo $custom_likes->post_likes( '' );
                }
                 ?>

                <!-- <div class="social">
                  <p>Compartilhe</p>
                    GranoSocialShare(get_the_permalink())
                </div> -->

                <!-- Author -->
                <!-- <p class="lead">
                    by <a href="<?php //echo get_author_posts_url(get_the_author_meta( 'ID' )); ?>"><?php echo get_the_author(); ?></a>
                </p> -->


                <!-- Post Content -->
                <?php the_content(); ?>

                <hr>

                <!-- Date/Time -->
								<?php $data_atualização = get_the_modified_date(); ?>

                <p class="data"><span class="fa fa-clock-o"></span> <?php echo get_the_date(); ?> | <?php echo !empty($data_atualização) ? "Atualizado dia ".$data_atualização : ''; ?></p>
                <!-- <div class="social">
                  <p>Compartilhe</p>
                     GranoSocialShare(get_the_permalink())
                </div> -->

                <!-- Posicionando o jetpack em uma posição específica -->
                <?php
                if ( function_exists( 'sharing_display' ) ) {
                    sharing_display( '', true );
                }

                if ( class_exists( 'Jetpack_Likes' ) ) {
                    $custom_likes = new Jetpack_Likes;
                    echo $custom_likes->post_likes( '' );
                }
                 ?>

                <div class="fb-comments" href="<?php the_permalink(); ?>" data-numposts="5"></div>
                <br>

                <!-- Blog Comments -->

            </div>
					<?php endwhile; // end of the loop. ?>

            <!-- Blog Sidebar Widgets Column -->
            <?php get_sidebar('blog'); ?>


        </div>
        <!-- /.row -->


    </div>
    <div class="cadastrar" style="margin-top:-7px;">
      <p>Cadastre-se para ter acesso a conteúdos exclusivos</p>
    </div>

    <?php echo do_shortcode('[mc4wp_form id="282"]'); ?>
    <!-- /.container -->

<?php get_footer(); ?>
