<?php
/**
 * Grano Studio functions and definitions
 *
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
 

add_theme_support( 'post-thumbnails' );


// offset the main query on the home page
function tutsplus_offset_main_query ( $query ) {
     if ( $query->is_home() && $query->is_main_query() ) {
         $query->set( 'offset', '1' );
    }
 }


 //  O número 80 é a quantidade de caracteres a exibir.
function the_excerpt_max_charlength($charlength) {
		$excerpt = get_the_excerpt();
		$charlength++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '...';
		} else {
			echo $excerpt;
		}
	} 


function granostudio_scripts_child() {

 //Desabilitar jquery
 wp_deregister_script( 'jquery' );  

 // Theme stylesheet.
 wp_enqueue_style( 'granostudio-style', get_stylesheet_uri()  );
 // import fonts (Google Fonts)
 wp_enqueue_style('granostudio-style-fonts', get_stylesheet_directory_uri() . '/css/fonts/fonts.css');
 // Theme front-end stylesheet
 wp_enqueue_style('granostudio-style-front', get_stylesheet_directory_uri() . '/css/main.min.css');

 // scripts js
 wp_enqueue_script('granostudio-scripts', get_stylesheet_directory_uri() . '/js/dist/scripts.min.js', '000001', false, true);

 // network effet home

}
add_action( 'wp_enqueue_scripts', 'granostudio_scripts_child' );
