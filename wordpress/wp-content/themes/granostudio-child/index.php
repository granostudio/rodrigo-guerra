<?php get_header(); ?>
<!-- 
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>rodrigo-guerra</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,700">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Search.css">
    <link rel="stylesheet" href="assets/css/rodrigoguerra.compiled.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>
 -->

<div class="container-fluid pulse animated" id="banner-home">
    <div class="row">
        <div class="col-md-6 col-md-offset-6"><img src="<?php echo get_stylesheet_directory_uri();?>/img/logo.png" style="padding:0px;width:100%;">
            <h3 class="text-center"><i class="icon ion-minus-round" style="color:#4f6266;"></i>BLOG<i class="icon ion-minus-round" style="color:#4f6266;"></i></h3>            
        </div>
    </div>
</div>

<div class="container" id="destaque">
    <div class="row">
        <div class="col titulo-destaque">
            <h2>DESTAQUE</h2>
            <div class="separador-titulo"></div>
        </div>
    </div>
    <div class="row no-gutters box-destaque-geral">

        <?php
         $args = array( 'post_type' => 'post', 'posts_per_page' => 1, 'category_name' => 'Destaque');
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

        <div class="col-12 col-sm-12 col-md-7 col-md-push-5 imagem-detaque" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>

        <div class="col-12 col-sm-12 col-md-5 col-md-pull-7 noticia-destaque">
            <h3 style="color:#24302e;">
                <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
            </h3>
            <ul class="lista-categoria">
            <?php
              foreach((get_the_category()) as $category) {
                echo '<li class="hashtags">' . $category->cat_name . '</li>';
              }
             ?>
            </ul> 
            <p><?php echo the_excerpt_max_charlength(750); ?><br></p>
            <a href="<?php echo get_the_permalink(); ?>" class="btn-destaque">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/right-arrow.png" class="">
            </a>
        </div>

        <?php endwhile; // end of the loop. ?>
        <?php endif; ?>

    </div>
</div>

<div class="container" data-aos="fade" data-aos-duration="1000" id="postagens">
    
    <div class="row">

        <div class="col-sm-12">
            <div class="cabecalho">
                <h2>CATEGORIAS</h2>
                <div class="separador-titulo"></div>
            </div>

            <div class="categorias">
                <?php wp_list_categories( array(
                    'orderby'    => 'name',
                    'title_li' => false
                ) ); ?> 
            </div>

            <form class="form-inline mr-auto form-pesquisa" role="search" method="get" id="searchform">
                <div class="form-group">
                    <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
                    <label for="search-field">
                        <i class="fa fa-search"></i>
                    </label>
                </div>
            </form>

        </div>           

    </div>

    <div class="row box-postagem-geral">

        <?php
         $args = array( 'post_type' => 'post', 'posts_per_page' => 6);
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

        <div class="col-12 col-sm-6 col-md-4 noticia-postagem">
            <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
            <ul class="lista-categoria">
            <?php
              foreach((get_the_category()) as $category) {
                echo '<li class="hashtags">' . $category->cat_name . '</li>';
              }
             ?>
            </ul>
            <p><a href="<?php echo get_the_permalink(); ?>"><?php echo the_excerpt_max_charlength(200); ?></a><br></p>
            <a href="<?php echo get_the_permalink(); ?>" class="btn-destaque">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/right-arrow.png" class="">
            </a>
            <div class="segmet"></div>
            <div class="segmet"></div>
        </div>

        <?php endwhile; // end of the loop. ?>
        <?php endif; ?>
        
    </div>

</div>

<?php get_footer(); ?>
 