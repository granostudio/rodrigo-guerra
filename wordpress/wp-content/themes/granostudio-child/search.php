<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>


<?php if (is_search()) { ?> 
  
<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>

<!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="" style="margin-top: 140px;">

                <div>
                    <h1 class="page-header header-search">
                        Você pesquisou por: 
                        <?php printf( __( '<span class="glyphicon glyphicon-search" aria-hidden="true"></span> %s', 'twentysixteen' ), '<span style="color: #0D2D25;">' . esc_html( get_search_query() ) . '</span>' ); ?>
                    </h1>
                    
                </div>

                <div class="box-postagem-geral">

                    <?php
                    if( have_posts() ) {
                      while ( have_posts() ) {
                        the_post(); ?>
                        
                        <div class="col-12 col-sm-6 col-md-4 noticia-postagem">
                            <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
                            <ul class="lista-categoria">
                            <?php
                              foreach((get_the_category()) as $category) {
                                echo '<li class="hashtags">' . $category->cat_name . '</li>';
                              }
                             ?>
                            </ul>
                            <p><a href="<?php echo get_the_permalink(); ?>"><?php echo the_excerpt_max_charlength(200); ?></a><br></p>
                            <a href="<?php echo get_the_permalink(); ?>" class="btn-destaque">
                                <img src="<?php echo get_stylesheet_directory_uri();?>/img/right-arrow.png" class="">
                            </a>
                            <div class="segmet"></div>
                            <div class="segmet"></div>
                        </div>                                               


                      <?php } ?>

                        <!-- Pager -->
                        <ul class="pager">

                            <li class="previous"><?php next_posts_link( 'Older posts' ); ?></li>
                            <li class="next"><?php previous_posts_link( 'Newer posts' ); ?></li>

                        </ul>

                        <div style="margin: 0px 15px;">
                            <h4 style="float: left;">Não encontrou o que desejava?</h4>
                            <form class="form-inline mr-auto form-pesquisa form-pesquisa-erro" role="search" method="get" id="searchform">
                                <div class="form-group">
                                    <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
                                    <label for="search-field">
                                        <i class="fa fa-search"></i>
                                    </label>
                                </div>
                            </form>
                        </div>

                    <?php } else { ?>

                        <div class="else-busca">
                    
                            <h4 style="float: left;">Não há resultados encontrados, tente fazer uma nova pesquisa:</h4>

                            <form class="form-inline mr-auto form-pesquisa form-pesquisa-erro" role="search" method="get" id="searchform">
                                <div class="form-group">
                                    <input class="form-control search-field" type="search" placeholder="Pesquisar..." name="s" id="s">
                                    <label for="search-field">
                                        <i class="fa fa-search"></i>
                                    </label>
                                </div>
                            </form>

                        </div>

                    <?php } ?>
                    
                </div>

            </div>

        </div>
        <!-- /.row -->

        <hr>


    </div>
    <!-- /.container -->

<?php get_footer(); ?>
