<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */

get_header(); ?>

<?php if (is_single()) { ?> 
  
<style type="text/css">
  .navbar{
    margin-top: 0px !important;
  }
</style>

<?php } ?>

<!-- <div class="container-fluid pulse animated" id="header-interna" style="padding:0px 0px;">
    <div class="row" style="margin:0px;">
    
        <div class="col-12 col-md-3 col-lg-3"><img class="img-fluid" src="<?php echo get_stylesheet_directory_uri();?>/img/logo-2.png"></div>
        <div class="col-12 col-md-4 col-lg-4 offset-0 offset-md-4 offset-lg-4">
            <p class="text-center" style="font-family:Muli, sans-serif;color:rgb(255,255,255);font-size:12px;">Siga-nos em nossas mídias sociais&nbsp;<a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-instagram"></i></a><a href="#"><i class="fa fa-linkedin"></i></a></p>
        </div>
    </div>
</div> -->

<div class="container">
    
    <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
 
    <div class="row margin-t-20">      
        <div class="col">
            <h1 style="font-family:Muli, sans-serif;"><?php echo get_the_title(); ?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col post-img" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>        
    </div>
    <div class="row post-text" data-aos="fade">
        <div class="col" style="font-family:Muli, sans-serif;font-size: 16px;">
            <?php the_content(); ?>
        </div>
    </div>

    <?php endwhile; // end of the loop. ?>
</div>

<div class="container" data-aos="fade" data-aos-duration="1000" id="postagens">
    <div class="row">
        <div class="col-12" style="padding:17px 15px;">
            <h2>LEIA TAMBÉM</h2>
            <div class="separador-titulo"></div>
        </div>
    </div>
    <div class="row box-postagem-geral">

        <?php
         $args = array( 'post_type' => 'post', 'posts_per_page' => 3);
         $loop = new WP_Query( $args );

         if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); $post_id = get_the_ID(); // run the loop ?>

        <div class="col-12 col-sm-6 col-md-4 noticia-postagem">
            <h3><a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a></h3>
            <ul class="lista-categoria">
            <?php
              foreach((get_the_category()) as $category) {
                echo '<li class="hashtags">' . $category->cat_name . '</li>';
              }
             ?>
            </ul>
            <p><a href="<?php echo get_the_permalink(); ?>"><?php echo the_excerpt_max_charlength(200); ?></a><br></p>
            <a href="<?php echo get_the_permalink(); ?>" class="btn-destaque">
                <img src="<?php echo get_stylesheet_directory_uri();?>/img/right-arrow.png" class="">
            </a>
            <div class="segmet"></div>
            <div class="segmet"></div>
        </div>

        <?php endwhile; // end of the loop. ?>
        <?php endif; ?>
        
    </div>
</div>

<?php get_footer(); ?>
