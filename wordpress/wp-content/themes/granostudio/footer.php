<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Grano Studio
 * @since Grano Studio 1.0
 */
?>
<?php wp_footer(); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/aos/2.1.1/aos.js"></script>
<script src="jquery.min.js"></script>
<script src="owlcarousel/owl.carousel.min.js"></script>

<?php
// livereload
$host = $_SERVER['HTTP_HOST'];
if ($host == "localhost:9001") {
  ?>
<script src="http://localhost:35729/livereload.js"></script>
  <?php
}

// /livereload
 ?>


<!-- FOOTER ============================================================== -->

<footer>

        <div class="container-fluid" data-aos="fade" style="padding:0px;">
          <div class="row" style="margin:0px 0px;">
            <div class="col-12 col-md-12 col-lg-6 formulario">
              <h3>Fale conosco</h3>
<!--               <div class="form-group">
                <input type="text" placeholder="Nome">
                <input type="email" placeholder="Email">
                <input type="tel" placeholder="Telefone">
              </div>
              <textarea placeholder="Mensagem"></textarea>
              <button class="btn btn-primary" type="submit">Enviar</button> -->

              <?php echo do_shortcode('[contact-form-7 id="18" title="Fale conosco"]'); ?>
              
            </div>

            <div class="col-12 col-md-12 col-lg-6 newsletter">
              <h3>Assine nossa newsletter</h3>
              <div class="form-group">
                <input type="email" placeholder="Email"><button class="btn btn-primary" type="button">Cadastrar</button>
              </div>
              <p style="font-size:16px;">Siga nos em nossas mídias</p>
              <p><a href="#"><i class="fa fa-facebook" style="font-size:30px;"></i></a><a href="#"><i class="fa fa-twitter" style="font-size:30px;"></i></a><a href="#"><i class="fa fa-instagram" style="font-size:30px;"></i></a><a href="#"><i class="fa fa-linkedin" style="font-size:30px;"></i></a></p>
              <p style="font-size:10px;">Desenvolvido por <a href="http://granostudio.com.br/" class="link-grano">Grano Studio</a></p>
            </div>
          </div>
        </div>


  <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Clique para retornar ao topo da página" data-toggle="tooltip" data-placement="left"><span class="fa fa-angle-up"></span></a>
</footer>

<!-- /FOOTER ============================================================== -->
</div><!-- /.container -->



<!-- Google Analytics -->
<?php $seoanalytics = grano_get_options('seo','seo_analytics');

if(!empty($seoanalytics) || $seoanalytics != "Default Text"){
  echo "<script>";
  echo $seoanalytics;
  echo "</script>";
}

 ?>
<!-- / Google Analytics -->
</div> <!-- / app angular -->
</body>



<!-- Preloader -->
<script type="text/javascript">
    // //<![CDATA[
    //     $(window).on('load', function() { // makes sure the whole site is loaded
    //         $('#status').fadeOut(); // will first fade out the loading animation
    //         $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    //         $('body').delay(350).css({'overflow':'visible'});
    //     })
    // //]]>
</script>

</html>
