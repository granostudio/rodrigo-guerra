<?php
/**
 * Grano Site Social Share
 *
 * @author    Grano studio
 */

/**
 *
 */
function GranoSocialShare($urlPost){
  $footerScript = false;

  // facebook
  ?>
  <!-- Your share button code -->
    <!-- <div class="fb-share-button"
      data-href="<?php// echo $urlPost; ?>"
      data-layout="button">Facebook
    </div> -->
    <a href="http://www.facebook.com/share.php?u=<?php echo $urlPost; ?>" class="share facebook" target="_blank">Fecebook</a>
  <?php

  // twitter
  ?>
  <!-- <a href="https://twitter.com/share" class="twitter-share-button" data-url="<?php //echo $urlPost; ?>" data-hashtags="gessaude" data-lang="pt" data-show-count="false">
    Tweet
  </a> -->
  <a href="http://twitter.com/intent/tweet?status=<?php echo $urlPost; ?>" class="share twitter" target="_blank">Twitter</a>
  <?php

  // linkedin
  ?>
  <!-- <script type="IN/Share" data-url="<?php //echo $urlPost; ?>"></script> -->
  <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $urlPost; ?>" class="share linkedin" target="_blank">Linkedin</a>
  <?php

  // footerScripits
  if(!$footerScript){
    add_action( 'wp_footer', 'addFacebookScript' );
    add_action( 'wp_footer', 'addTwitterTwitter' );
    add_action( 'wp_footer', 'addLinkedin' );
  }

}

function addFacebookScript(){
  ?>
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=160350657363676";
      fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>
  <?php
}

function addTwitterTwitter(){
  ?>
  <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
  <?php
}

function addLinkedin(){
  ?>
  <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: pt_BR</script>
  <?php
}
