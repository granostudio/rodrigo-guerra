<?php
/**
* Page Template
*
* @package WordPress
* @subpackage Grano Studio
* @since Grano Studio 1.0
 */

get_header();?>

<?php

// page

while ( have_posts() ) : the_post();

	$group = get_post_meta( get_the_ID(), 'page_layout', true );
	// print_r($group);

	foreach ( (array) $group as $key => $entry ) {
		// modulo Banner
		if($entry['page_modulo']=='Banner'){
			$bannerfotos = $entry['page_bannerfotos'];
			module_bannerfotos($bannerfotos, $key);
		}
		// modulo Banner com coteudo
		if($entry['page_modulo']=='Bannerconteudo'){
			$bannerconteudo = $entry['page_bannerconteudo'];
			module_bannerconteudo($bannerconteudo, $key);
		}
		// modulo Contato
		if($entry['page_modulo']=='Contato'){
			$contato = $entry['page_formcontato'];
			module_contato($contato,$key);
		}
		// modulo Equipe
		if($entry['page_modulo']=='Equipe'){
			module_equipe();
		}
		// modulo Externo
		if($entry['page_modulo']=='Externo'){
			$modeloexterno = $entry['page_modeloexterno'];
			if(!empty($modeloexterno)){
					if (is_child_theme()) {
						include 'wp-content/themes/granostudio-child/modules/'.$modeloexterno;
					} else {
						include 'modules/'.$modeloexterno;
					}
			}else{
					echo "Nome do arquivo do módulo não preenchido!";
			}
		}
		// modulo Posts
		if($entry['page_modulo']=='Pots'){
			module_posts();
		}
		// modulo Posts
		if($entry['page_modulo']=='Portfolio'){
			module_portfolio();
		}
		// modulo Foto com texto
		if($entry['page_modulo']=='Fotocomtexto'){
			$fotocomtexto_pos   = $entry['page_fotocomtexto_pos'];
			$fotocomtexto_id    = $entry['page_fotocomtexto_foto_id'];
			$fotocomtexto_texto = $entry['page_fotocomtexto_texto'];
			module_fotocomtexto($fotocomtexto_id, $fotocomtexto_texto, $fotocomtexto_pos, $key);
		}
		// modulo texto com botao
		if($entry['page_modulo']=='Textocombotao'){
			$textocombotao_titulo			  = $entry['page_textocombotao_titulo'];
			$textocombotao_conteudo     = $entry['page_textocombotao_conteudo'];
			$textocombotao_link_titulo  = array_key_exists('page_textocombotao_link_titulo',$entry) ? $entry['page_textocombotao_link_titulo'] : '';
			$textocombotao_link_url	    = array_key_exists('page_textocombotao_link_titulo',$entry) ? $entry['page_textocombotao_link_url'] : '';
			module_textocombotao($textocombotao_titulo, $textocombotao_conteudo, $textocombotao_link_titulo,$textocombotao_link_url, $key);
		}

	}


	// End of the loop.
endwhile;

 ?>

<?php get_footer(); ?>
